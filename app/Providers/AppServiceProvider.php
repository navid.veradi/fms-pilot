<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(env('FMS_INSTALLED')){
            $vendor = env('FMS_VENDOR');
            $package = env('FMS_PACKAGE');
            $this->app->register("Asropaten\\FMS\\FmsServiceProvider");
            if(file_exists(base_path("packages/$vendor/$package/".$package."ServiceProvider.php"))) {
                $this->app->register("$vendor\\$package\\".$package."ServiceProvider");
            }
        }
        else {
            $this->app->register("Asropaten\\FMS_Installer\\InstallServiceProvider");
            if(env('FMS_AVAILABLE')) {
                $this->app->register("Asropaten\\FMS\\FmsServiceProvider");
            }
        }
    }
}
