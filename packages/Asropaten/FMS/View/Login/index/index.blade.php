@extends('Loggo::auth')

@section('content')
	<div class="TILES -MIDDLE -padding-top-first -padding-bottom-first">
		<div class="TILES -MIDDLE -height-second -padding-first" style="background-image: url('/Data/Asropaten/FMS/App/logo/1.svg');background-repeat: no-repeat;background-position: center;background-size:50%;">
		</div>
		<div class="_TILE _1_2 _responsive">
			<div class="-bg-blue -white -padding-third">
				Please <strong>login</strong> with your data
			</div>
			<form id="login_form" class="form" action="/login" method="post">
				<input name="_token" type="hidden" value="{{csrf_token()}}">
				<!--
				<div class="_title _TILE _1_1 -padding-third">
				<span>Login</span>
				</div>
				-->
				<div class="_elements -light -padding-third TILES">
					<div class="_element">
						<label for="text">E-mail <span class="-dark">*</span></label>
						<input name="email" id="text" type="email">
					</div>
					<div class="_element">
						<label for="password">Password <span class="-dark">*</span></label>
						<input name="password" id="password" type="password">
						<p class="-dark"><a href="#">Forgot your password?</a></p>
					</div>
					<div class="_element TILES">
						<button class="-dark _TILE _1_4 -right">Login</button>
					</div>
				</div>
				<input name="TtyHJoZF" value="NAcxUlwXpyoMITi8dO2e" type="hidden">
			</form>
		</div>
	</div>
@endsection
