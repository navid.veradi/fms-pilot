@extends('Loggo::index')

@section('content')
    <div class="TILES">
        <div class="_TILE -AUTO -margin-bottom-third">
            <h1>Dashboard</h1>
        </div>
        <div class="_TILE -FLEX TILES -margin-top-second">
            <form class="form _TILE -RIGHT">
                <div class="_element">
                    <a href="/fms/app/update/{{$app->id}}" class="_button -light -big -width-fourth -padding-fourth -center"><span>Update</span></a>
                </div>
            </form>
        </div>
    </div>
    <hr class="-margin-top-zero">
    <div class="TILES">
        <div class="_TILE _1_1">
            <p class="-bold">Main Information</p>
            <div class="read">
                <div class="TILES">
                    <div class="_TILE _1_4">Name [domain]</div>
                    <div class="_TILE _3_4">{{$app->name}} [<a href="{{$app->domain}}">{{$app->domain}}</a>]</div>
                    <div class="_TILE _1_4">Slogan</div>
                    <div class="_TILE _3_4">Slogan</div>
                    <div class="_TILE _1_4">Description</div>
                    <div class="_TILE _3_4">Description</div>
                    <div class="_TILE _1_4">created</div>
                    <div class="_TILE _3_4">23. Juni 2016 by Milad Majidi</div>
                    <div class="_TILE _1_4">Version:</div>
                    <div class="_TILE _3_4">
                        {{ $app->module('update_status', ['updater'=>$updater]) }}
                    </div>
                    <div class="_TILE _1_4">License</div>
                        {{ $app->module('license_status', ['updater'=>$updater]) }}
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="TILES">
        <div class="_TILE _1_2">
            <p class="-bold">Statistics</p>
            <div class="read">
                <div class="TILES">
                    <div class="_TILE _1_2">Users</div>
                    <div class="_TILE _1_2">23</div>
                    <div class="_TILE _1_2">Groups</div>
                    <div class="_TILE _1_2">3</div>
                    <div class="_TILE _1_2">Visits</div>
                    <div class="_TILE _1_2">5464</div>
                    <div class="_TILE _1_2">Registrations</div>
                    <div class="_TILE _1_2">654</div>
                    <div class="_TILE _1_2">Pending</div>
                    <div class="_TILE _1_2 -red">23</div>
                    <div class="_TILE _1_2">Your Messages</div>
                    <div class="_TILE _1_2 -green">5</div>
                </div>
            </div>
        </div>
        <div class="_TILE _1_2">
            <p class="-bold">Activities<a class="pull-xs-right -lighter -light">refresh</a></p>
            <div class="TILES info -padding-bottom-fifth">
                <div class="_TILE _avatar">
                    {{ $fmsUser->module('small_avatar') }}
                </div>
                <div class="_TILE -FLEX -padding-left-fourth _content -padding-top-fifth-negativ">
                    <p><span class="-bold">Milad</span> creadted the Campaign <span class="-italic">Best IDE 2014</span> (19)</p>
                    <p><span class="-light_on_dark">19 min ago</span> <span class="-light">•</span> <a href="">delete</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection
