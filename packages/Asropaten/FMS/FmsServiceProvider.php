<?php

namespace Asropaten\FMS;

use Illuminate\Support\ServiceProvider;

class FmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set views DIR
        $this->loadViewsFrom(__DIR__.'/View', 'FMS');
        $this->loadViewsFrom(__DIR__.'/Layout/Loggo/View', 'Loggo');
        $this->loadViewsFrom(base_path(). '/packages/', 'Module');
        //Publish Asset
        $this->publishes(
            [
                __DIR__.'/Asset' => public_path('Asset/Asropaten/FMS'),
                __DIR__.'/Layout/Loggo/Asset' => public_path('Asset/Asropaten/FMS')
            ],
            'Asset'
        );
        // Publish Data
        $this->publishes(
            [
                __DIR__.'/Data/Public' => public_path('Data/Asropaten/FMS'),
                __DIR__.'/ProvidingData/install-mac.sh' => base_path('install-mac.sh')
            ],
            'Data');
        // Publish Config
        $this->publishes(
            [
                __DIR__.'/Config' => config_path()
            ],
            'Config'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        include __DIR__.'/routes.php';
        $this->app['router']->middleware('FmsAuth', 'Asropaten\FMS\Middleware\FmsAuth');
    }
}
