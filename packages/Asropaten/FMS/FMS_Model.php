<?php

namespace Asropaten\FMS;

use Asropaten\FMS\Base_Model;
use Asropaten\FMS\Model\Package;

class FMS_Model extends Base_Model
{
    protected $prefix = "fms_";

    function __construct(){
        parent::__construct();
        $fmsPackage = Package::where('name', '=', 'FMS')->first();
        $this->prefix = $fmsPackage->shortcut . "_";
    }
}