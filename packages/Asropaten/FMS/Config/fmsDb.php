<?php
return [
    /*
     * FMS Database Sync
     * Create Your Tables Here
     * First level keys are table names
     * Second level keys are field names and values are field types
     * For "foreign" Value array , each key name is field name and it has a value array with keys: "references","on" and "onDelete"
     * See laravel's migration docs for more info
     */
    'fms_test' => [
        'id' => 'increments',
        'user_id' => 'integer',
        'name' => 'string',
        'timestamps' => 'true',
        'foreign' => [
            'user_id' => [
                'references' => 'id',
                'on' => 'fms_users',
                'onDelete' => 'cascade'
            ]
        ]
    ],
];