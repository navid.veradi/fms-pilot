<?php
return [
    /* FMS Config file.
       FMS Version and other future configurations are stored here. */

    /* FMS Version */
    'version' => '0.0.81',

    /* License :
        License provided by Asropaten! */
    'license' => 'FMS657f9623969b8544595fadec132b243f',
    /* DB Prefix : */
    'db_prefix' => 'fms_',
    /* DB Head */
    'db_head' => '1471871353',
];
