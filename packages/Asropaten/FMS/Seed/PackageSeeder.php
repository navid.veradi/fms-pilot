<?php

namespace Asropaten\FMS\Seed;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class PackageSeeder extends Seeder
{
    // php artisan migrate:refresh --path=packages/Asropaten/FMS/Migration 
    // php artisan db:seed --class=Asropaten\\FMS\\Seed\\FMSSeeder
    /**
     * Run the Package seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fms_layouts')->insert([
            'name'         => 'Loggo',
        ]);
        DB::table('fms_apps')->insert([
            'name'              => 'app',
            'domain'            => 'fms.app'
        ]);
        DB::table('fms_packages')->insert([
            'namespace'         => 'Asropaten\\',
            'name'              => 'FMS',
            'app_id'            => 1,
            'shortcut'          => 'fms',
            'layout_id'         => 1,
        ]);
        DB::table('fms_models')->insert([
            'package_id'        => 1,
            'class'             => 'Asropaten\\FMS\\Model\\App',
            'title'             => 'App',
            'name'              => 'app',
            'plural'            => 'apps',
            'table_name'        => 'apps'
        ]);
        DB::table('fms_models')->insert([
            'package_id'        => 1,
            'class'             => 'Asropaten\\FMS\\Model\\FmsUser',
            'title'             => 'FmsUser',
            'name'              => 'fmsUser',
            'plural'            => 'fmsusers',
            'table_name'        => 'users'
        ]);
        DB::table('fms_controllers')->insert([
            'package_id'        => 1,
            'name'              => 'dashboard',
            'title'             => 'Dashboard',
            'class'             => 'DashboardController'
        ]);
        DB::table('fms_actions')->insert([
            'controller_id'     => 1,
            'name'              => 'index',
            'title'             => 'Index',
            'with_layout'       => 1,
            'active'            => 1
        ]);
        DB::table('fms_users')->insert([
            'name'              => 'Admin',
            'email'             => 'admin@admin.com',
            'password'          =>  \Hash::make('password')
        ]);
    }
}
