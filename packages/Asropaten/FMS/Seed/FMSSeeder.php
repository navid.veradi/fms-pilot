<?php

namespace Asropaten\FMS\Seed;

use Illuminate\Database\Seeder;

class FMSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PackageSeeder::class);
    }
}
