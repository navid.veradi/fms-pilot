<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Asropaten\FMS\Model\Package;

Route::group(['middleware' => ['web']], function () {
    Route::get(
        'login',
        "Asropaten\\FMS\\Controller\\LoginController@index"
        );
    Route::post(
        'login',
        "Asropaten\\FMS\\Controller\\LoginController@attempt"
        );
    Route::group(['middleware' => ['FmsAuth']], function(){
        Route::get(
            'logout',
            function(){
                Auth::guard('fms_user')->logout();
                return redirect('login');
                }
            );
        Route::get(
            'update',
            "Asropaten\\FMS\\Controller\\DashboardController@update"
            );
    });

    Route::match(
        ['get','post'],
        "/{packageName}/{controllerName}/{actionName}",
        function($packageName, $controllerName, $actionName){
            $package =      Package::where('name','=',$packageName)->first();
            $controller =   $package->controllers->where('name', $controllerName)->first();
            $action =       $controller->actions->where('name', $actionName)->first();
            if($package)
                if($controller)
                    if($action){
                        $class = $controller->getClassPath();
                        $mainController = new $class();
                        return $mainController->handle($package, $controller, $action);
                    }
                    else die('Package is missing.');
                else die('Controller is missing.');
            else die('Action is missing.');
        }
        );
});