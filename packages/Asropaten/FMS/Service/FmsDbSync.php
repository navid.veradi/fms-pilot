<?php
/**
 * Created by PhpStorm.
 * User: Navid
 * Date: 9/18/2016 AD
 * Time: 12:54 PM
 */

namespace Asropaten\FMS\Service;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class FmsDbSync
{
    protected $dbConf = [];
    public function sync(){
        $this->dbConf = include(base_path().'/packages/Asropaten/FMS/Config/fmsDb.php');
        $this->up();
        //$this->down();
    }
    private function up(){
        foreach (array_keys($this->dbConf) as $tableName) {
            if(!Schema::hasTable($tableName)){
                Schema::create($tableName, function (Blueprint $table) {
                    foreach($this->dbConf as $tableKey => $tableValue){
                        foreach($tableValue as $key => $value){
                            if($key !== "timestamps" && $key !== "foreign"){
                                $valueArray = explode("|",$value);
                                if($valueArray[1] === "unsigned"){
                                    if(!Schema::hasColumn($tableKey,$key)){
                                        $table->{$valueArray[0]}($key)->unsigned();
                                    }
                                }
                                else {
                                    if(!Schema::hasColumn($tableKey,$key)){
                                        $table->{$valueArray[0]}($key);
                                    }
                                }
                            }
                            else if($key === "timestamps"){
                                if($value === "true")
                                    $table->timestamps();
                            }
                            else if($key === "foreign"){
                                foreach($value as $foreignKey => $constraint){
                                    $table->foreign($foreignKey)
                                        ->references($constraint["references"])->on($constraint["on"])
                                        ->onDelete($constraint["onDelete"]);
                                }
                            }
                        }
                    }
                });
            }
            else {
                Schema::table($tableName, function ($table) {
                    foreach($this->dbConf as $tableKey => $tableValue){
                        foreach($tableValue as $key => $value){
                            if($key !== "timestamps" && $key !== "foreign"){
                                $valueArray = explode("|",$value);
                                if($valueArray[1] === "unsigned"){
                                    if(!Schema::hasColumn($tableKey,$key)){
                                        $table->{$valueArray[0]}($key)->unsigned();
                                    }
                                }
                                else {
                                    if(!Schema::hasColumn($tableKey,$key)){
                                        $table->{$valueArray[0]}($key);
                                    }
                                }
                            }
                            else if($key === "timestamps"){
                                if(!$table->hasColumn("created_at"))
                                    if($value === "true")
                                        $table->timestamps();
                            }
                        }
                    }
                });
            }
        }

    }
    private function down(){
        // Checking tables
        $dbTables = DB::select('SHOW TABLES');
        foreach($dbTables as $dbTable){
            if(!array_key_exists($dbTable, $this->dbConf)){
                Schema::drop($dbTable);
            }
        }
        // Checking columns
        foreach($this->dbConf as $key => $value){
            $dbCols = DB::getSchemaBuilder()->getColumnListing();
            $confCols = array_keys($value);
            foreach ($dbCols as $dbCol) {
                if(!in_array($dbCol, $confCols)){
                    DB::raw("ALTER TABLE ".$key." DROP COLUMN ".$dbCol);
                }
            }
        }
    }
}