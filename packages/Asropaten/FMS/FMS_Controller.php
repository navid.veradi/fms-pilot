<?php
namespace Asropaten\FMS;
use Asropaten\FMS\Base_Controller;
use Asropaten\FMS\Model\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Asropaten\FMS_Installer\Service\Enver;

class FMS_Controller extends Base_Controller
{
    function __construct()
    {
        $enver= new Enver('.env');

        $configDbHead = intval(config("fms.db_head"));
        $envDbHead = intval($enver->existKey("DB_HEAD"));
        $envVariables = $enver->getVariables();

        if($configDbHead !== $envDbHead){
            if($configDbHead > $envDbHead){
                Artisan::call('migrate', [
                    '--path' => 'packages/Asropaten/FMS/Migration/'
                ]);

                if($enver->existKey("FMS_ADMIN_PASSWORD")){
                    $user = new User();
                    $user->email = $envVariables['FMS_ADMIN_EMAIL'];
                    $user->password = Hash::make($envVariables['FMS_ADMIN_PASSWORD']);
                    $user->save();

                    $enver->deleteVariable("FMS_ADMIN_PASSWORD");
                }
            }
        }
    }
}