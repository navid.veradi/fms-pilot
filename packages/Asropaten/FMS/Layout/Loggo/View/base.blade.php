<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>App Name - @yield('title')</title>
        <meta name="description" content="Loggo">
        <meta name="author" content="Milad Majidi <milad.majidi@studios7.de>">
        <link rel="icon" href="/Data/Asropaten/FMS/App/icon/1.ico">

        <meta name="keywords" content="KEYWORDS">
        <meta name="language" content="en">
        <meta name="url" content="http://asropaten.com">

        <link rel="stylesheet" href="/Asset/Asropaten/FMS/Dist/bootstrap.min.css">
        <link rel="stylesheet" href="/Asset/Asropaten/FMS/Style.css">
        @yield('style')
    </head>
    <body id="package-controller-action">
        @yield('body')
        <script src="/Asset/Asropaten/FMS/Dist/jquery-1.11.3.min.js"></script>
        <script src="/Asset/Asropaten/FMS/Dist/bootstrap.min.js"></script>
        <script src="/Asset/Asropaten/FMS/Dist/tether.js"></script>
        @yield('script')
    </body>
</html>
