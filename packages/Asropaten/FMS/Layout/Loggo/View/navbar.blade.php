<nav class="navbar navbar-fixed-top">

    <div class="_hamburger">
        <button class="navbar-toggler hidden-sm-up collapsed" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header" aria-expanded="false">
            ☰
        </button>
    </div>

    <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
        <div class="container">

            <div class="TILES">
                <div class="_TILE">
                    <span class="navbar-brand -navbar_main -height-fourth">
                        <img class="_logo" style="max-height:32px;width:auto;" src="/Data/Asropaten/FMS/App/logo/1.png">
                        <img class="_logo_responsive -padding-top-fifth -padding-bottom-fifth -width-sixth" style="width:auto;margin:auto;" src="/Data/Asropaten/FMS/App/logo-responsive/1.png">
                    </span>
                </div>

                @if($fmsUser)
                    {{ $fmsUser->module('navbar_widget') }}
                    {{-- <div class="_menu pull-xs-right">
                        <div class="TILES menu -light">
                            <div class="_TILE _AUTO _h3 -active"><a href="#">Active</a></div>
                            <div class="_TILE _AUTO _h3 -seperator">|</div>
                            <div class="_TILE _AUTO _h3"><a href="/logout">Logout</a></div>
                        </div>
                    </div> --}}
                @else
                    <form class="form pull-xs-right -padding-right-zero -padding-third" method="post" action="/login">
                        <div class="_element">
                            <a class="_button -line -blue -center" href="/login"><span>Login</span></a>
                        </div>
                    </form>
                @endif


{{--                 <div class="_TILE -FLEX">
                    <ul class="nav navbar-nav">
                        @if($fmsUser)
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/#">Link</a>
                            </li>
                        @endif
                    </ul>
                </div> --}}
            </div>

        </div>
    </div>
</nav>
