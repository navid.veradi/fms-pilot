@extends('Loggo::base')

@section('body')
    <div class="container ">
        <div class="content">
            <main class="-no-sidebar -no-navbar">
                @yield('content')
                @include('Loggo::footer')
            </main>
        </div>
    </div>
@stop

@section('style')
    <style type="text/css">
        body {
            background-color: white;
        }
    </style>
@stop