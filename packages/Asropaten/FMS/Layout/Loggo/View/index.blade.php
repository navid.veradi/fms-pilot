@extends('Loggo::base')

@section('body')
    @include('Loggo::navbar')
    <div class="container ">
        <div class="content">
            <div class="sidebar">
                @include('Loggo::sidebarMain')
            </div>
            <main>
                @yield('content')
                @include('Loggo::footer')
            </main>
        </div>
    </div>
@stop
