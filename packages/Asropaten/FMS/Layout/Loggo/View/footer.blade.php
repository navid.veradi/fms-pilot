<footer class="TILES -padding-top-second -padding-bottom-second">
    <div class="_TILE -FLEX -padding-right-fourth text-xs-right">Made with</div>
    <div class="_TILE _heart"></div>
    <div class="_TILE -FLEX -padding-left-fourth text-xs-left">in Berlin.</div>
</footer>