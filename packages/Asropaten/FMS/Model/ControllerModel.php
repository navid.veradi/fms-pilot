<?php

namespace Asropaten\FMS\Model;

use Asropaten\FMS\FMS_Model;

class ControllerModel extends FMS_Model
{
    protected $classPath;

    function __construct(){
        $this->table = $this->prefix . "controllers";
    }

    public function package()
    {
        return $this->belongsTo('Asropaten\FMS\Model\Package');
    }

    public function actions()
    {
        return $this->hasMany('Asropaten\FMS\Model\Action', 'controller_id');
    }

    /**
     * @return mixed
     */
    public function getClassPath()
    {
        return $this->package->namespace . $this->package->name . "\\Controller\\" . $this->class;
    }

}
