<?php

namespace Asropaten\FMS\Model;

use Asropaten\FMS\FMS_Model;

class Package extends FMS_Model
{
    function __construct(){
        $this->table = $this->prefix . "packages";
    }

    public function controllers()
    {
        return $this->hasMany('Asropaten\FMS\Model\ControllerModel');
    }

    public function models()
    {
        return $this->hasMany('Asropaten\FMS\Model\Model');
    }

    public function getPathForAllModules()
    {
        return $this->namespace.$this->name.'\\Module\\';
    }
}
