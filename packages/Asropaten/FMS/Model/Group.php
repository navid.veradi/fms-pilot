<?php

namespace Asropaten\FMS\Model;

use Asropaten\FMS\FMS_Model;

class Group extends FMS_Model
{
    //
    function __construct(){
        $this->table = $this->prefix . "groups";
    }

    public function users(){
        return $this->belongsToMany('Asropaten\FMS\Model\User', $this->prefix . 'groups_users');
    }
}
