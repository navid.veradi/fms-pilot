<?php

namespace Asropaten\FMS\Model;

use Asropaten\FMS\FMS_Model;

class Model extends FMS_Model
{
    function __construct(){
        $this->table = $this->prefix . "models";
    }

    public function package()
    {
        return $this->belongsTo('Asropaten\FMS\Model\Package');
    }

    protected function getPathForModules()
    {
        return $this->package->getPathForAllModules().$this->title.'\\';
    }
}
