<?php

namespace Asropaten\FMS\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = "fms_users";

    protected $hidden = ["password"];

    public function groups(){
        return $this->belongsToMany('Asropaten\FMS\Model\Group', $this->prefix . 'groups_users');
    }
}
