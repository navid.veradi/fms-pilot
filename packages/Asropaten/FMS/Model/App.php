<?php

namespace Asropaten\FMS\Model;

use Asropaten\FMS\FMS_Model;

class App extends FMS_Model
{
    //
    function __construct(){
        $this->table = $this->prefix . "apps";
    }
}
