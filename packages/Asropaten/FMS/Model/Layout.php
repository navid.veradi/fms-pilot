<?php

namespace Asropaten\FMS\Model;

use Asropaten\FMS\FMS_Model;

class Layout extends FMS_Model
{
    //
    function __construct(){
        $this->table = $this->prefix . "layouts";
    }
}
