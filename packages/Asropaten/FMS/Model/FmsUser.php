<?php

namespace Asropaten\FMS\Model;

use Asropaten\FMS\FMS_Model;

class FmsUser extends FMS_Model
{
    //
    function __construct(){
        $this->table = $this->prefix . "users";
    }
    public function groups(){
        return $this->belongsToMany('Asropaten\FMS\Model\Group', $this->prefix . 'groups_users');
    }
}
