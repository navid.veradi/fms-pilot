<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class PackageModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('fms_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('layout_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->string('name');
            $table->string('title');
            $table->string('shortcut', 3);
            $table->string('redirect');
            $table->string('namespace');
            $table->timestamps();
            $table->foreign('layout_id')
                ->references('id')->on('fms_layouts')
                ->onDelete('cascade');
            $table->foreign('app_id')
                ->references('id')->on('fms_apps')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('fms_packages');
    }
}
