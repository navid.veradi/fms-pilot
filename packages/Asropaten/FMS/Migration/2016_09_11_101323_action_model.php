<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class ActionModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('fms_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('controller_id')->unsigned();
            $table->string('name');
            $table->string('title');
            $table->boolean('with_layout');
            $table->boolean('active');
            $table->timestamps();

            $table->foreign('controller_id')
                ->references('id')->on('fms_controllers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('fms_actions');
    }
}
