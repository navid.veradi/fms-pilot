#!/bin/bash

echo 'Default application name for development: fms'
read -p 'appkication name: ' appname

echo "Installing Dependencies"
mkdir bootstrap/cache
composer install
php artisan vendor:publish --tag Data
php artisan vendor:publish --tag Asset
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VMIP="192.168.60.60"
echo "Adding homestead host to /etc/hosts"

# insert/update hosts entry
ip_address=$VMIP
host_name=$appname".app"
# find existing instances in the host file and save the line numbers
matches_in_hosts="$(grep -n $host_name /etc/hosts | cut -f1 -d:)"
host_entry="${ip_address} ${host_name}"

echo "Please enter your password if requested."
if [ ! -z "$matches_in_hosts" ]
then
    echo "Updating existing hosts entry."
    # iterate over the line numbers on which matches were found
    while read -r line_number; do
        # replace the text of each line with the desired host entry
        sudo sed -i '' "${line_number}s/.*/${host_entry} /" /etc/hosts
    done <<< "$matches_in_hosts"
else
    echo "Adding new hosts entry."
    echo "$host_entry" | sudo tee -a /etc/hosts > /dev/null
fi
echo "Done."

echo "Creating Homestead.yaml"
>Homestead.yaml
echo '---' >>Homestead.yaml
echo 'ip: "'$VMIP'"' >>Homestead.yaml
echo 'memory: 2048' >>Homestead.yaml
echo 'cpus: 1' >>Homestead.yaml
echo 'hostname: "'$appname'"' >>Homestead.yaml
echo 'name: "'$appname'"' >>Homestead.yaml
echo $'provider: virtualbox\n' >>Homestead.yaml
echo $'authorize: ~/.ssh/id_rsa.pub\n' >>Homestead.yaml
echo 'keys:' >>Homestead.yaml
echo $'    - ~/.ssh/id_rsa\n' >>Homestead.yaml
echo 'folders:' >>Homestead.yaml
echo $'    - map: "'$BASEDIR'"' >>Homestead.yaml
echo $'      to: "/home/vagrant/'$appname'"' >>Homestead.yaml
echo 'sites:' >>Homestead.yaml
echo $'    - map: "'$appname'.app"' >>Homestead.yaml
echo $'      to: "/home/vagrant/'$appname'/public"' >>Homestead.yaml
echo 'databases:' >>Homestead.yaml
echo $'    - "'$appname'"' >>Homestead.yaml
echo '# blackfire:' >>Homestead.yaml
echo '#     - id: foo' >>Homestead.yaml
echo '#       token: bar' >>Homestead.yaml
echo '#       client-id: foo' >>Homestead.yaml
echo '#       client-token: bar' >>Homestead.yaml
echo '# ports:' >>Homestead.yaml
echo '#     - send: 50000' >>Homestead.yaml
echo '#       to: 5000' >>Homestead.yaml
echo '#     - send: 7777' >>Homestead.yaml
echo '#       to: 777' >>Homestead.yaml
echo '#       protocol: udp' >>Homestead.yaml
echo "Done."

echo "Creating .env"
>.env
echo "APP_ENV=local" >>.env
echo "APP_DEBUG=true" >>.env
echo "APP_KEY=base64:SrXvPxJhVaQJDzeEPGQSI9bdRuzxEEcAMN4i7RXeRfE=" >>.env
echo "APP_URL=http://$appname.app" >>.env
echo "DB_CONNECTION=mysql" >>.env
echo "DB_HOST=127.0.0.1" >>.env
echo "DB_PORT=3306" >>.env
echo "DB_DATABASE=$appname" >>.env
echo "DB_USERNAME=homestead" >>.env
echo "DB_PASSWORD=secret" >>.env
echo "CACHE_DRIVER=file" >>.env
echo "SESSION_DRIVER=file" >>.env
echo "QUEUE_DRIVER=sync" >>.env
echo "REDIS_HOST=127.0.0.1" >>.env
echo "REDIS_PASSWORD=null" >>.env
echo "REDIS_PORT=6379" >>.env
echo "MAIL_DRIVER=smtp" >>.env
echo "MAIL_HOST=mailtrap.io" >>.env
echo "MAIL_PORT=2525" >>.env
echo "MAIL_USERNAME=null" >>.env
echo "MAIL_PASSWORD=null" >>.env
echo "MAIL_ENCRYPTION=null" >>.env
echo "Done."
# etc.
