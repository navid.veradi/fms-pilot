<?php

namespace Asropaten\FMS\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FmsAuth
{
    public function handle($request, Closure $next)
    {
        if (Auth::guard('fms_user')->check()) {
            return $next($request);
        }
        else {
            return redirect('/login');
        }
    }
}