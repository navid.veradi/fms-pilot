<p>
    {{-- @inject('updater', 'Asropaten\FMS_Installer\Service\Updater') --}}
    {{ $updater->getApplicationVersion() }} -
    @if($updater->isUpdateAvailable())
        <span class="-red">{{ $updater->getAsropatenResponse()->message }}</span>
        <div class="form">
            <div class="_element">
                <a class="_button -line -blue -center" href="/update">
                    <span>Update to {{ $updater->getAsropatenResponse()->version }}</span>
                </a>
            </div>
        </div>
    @else
        <span class="-green">
            {{ $updater->getAsropatenResponse()->message }}
        </span>
    @endif
</p>