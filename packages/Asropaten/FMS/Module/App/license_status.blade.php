<div class="_TILE _3_4">{{ $updater->getLicense() }} - 
    @if($updater->isLicenseValid())
        <span class="-green">Valid</span>
    @else
        <span class="-green">Invalid</span>
    @endif
</div>