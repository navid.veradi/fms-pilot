<div class="_TILE -RIGHT">
    <div class="TILES">
        <div class="_TILE _0_0 -padding-third">
            {{ $fmsUser->module('small_avatar') }}
        </div>
        <div class="_TILE -FLEX -padding-bottom-third">
            <p class="-padding-bottom-third-negativ -gray -small text-uppercase">Hello <span class="-bold">{{ $fmsUser->name }}</span></p>
            <p class="-right -white">
                <span>Account</span><span class="-light"> • </span>
                <span><a href="/logout">Logout</a></span>
            </p>
        </div>
    </div>
</div>