<?php

namespace Asropaten\FMS\Controller;

use Asropaten\FMS\FMS_Controller;
use Asropaten\FMS_Installer\Service\Updater;

class DashboardController extends FMS_Controller
{

    public function index(){
        $updater = new Updater();
        $updater->setLicense(config('fms.license'));
        $updater->setAsropatenResponse();
        $this->addData('updater', $updater);
        // $app = $this->getApp();
        /*
        $this->addForm(
            'app_read',
            [
                'object'    => $app,
                // index, create, read, update, delete
                'icrud'     => 'read',
                'fields'    => [
                    'name_domain',
                    'slogan',
                    'description',
                    'created_by',
                    'version' => [
                        'isModule' => true
                    ],
                    'license' => [
                        'isModule' => true
                    ]
                ]
                // 'files' => []
                // 'redirect' => '#'
            ]
        );
        */
    }

    public function update(){
        $updater = new Updater();
        $updater->setLicense(config('fms.license'));
        $updater->setAsropatenResponse();

        if($updater->isUpdateAvailable()) {
            $updater->pull('Asropaten/FMS_Installer');
            $updater->pull('Asropaten/FMS');
        }
        return redirect('/fms/dashboard/index');
    }
}