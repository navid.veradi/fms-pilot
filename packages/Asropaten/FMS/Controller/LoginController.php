<?php
namespace Asropaten\FMS\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Asropaten\FMS\FMS_Controller;
use Asropaten\FMS\Model\User;

class LoginController extends FMS_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index(){
        if (Auth::guard('fms_user')->check()){
            return redirect('/fms/dashboard/index');
        }
        return view(
            "FMS::Login.index.index"
        );
    }

    public function attempt(Request $request){
        $this->validate($request,[
            "email" => "required|email",
            "password" => "required"
        ]);

        $password = $request->input('password');
        $user = User::where('email', '=', $request->input('email'))->first();
        if(Hash::check($password,$user->password))
        {
            Auth::guard('fms_user')->login($user);
            return redirect('/fms/dashboard/index');
        }
        else {
            return view(
                "FMS::Login.index.index",
                [
                    "errors" => [
                        "Email or Password was not found."
                    ]
                ]
            );
        }
    }
}
