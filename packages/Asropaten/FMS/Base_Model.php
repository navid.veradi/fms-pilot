<?php

namespace Asropaten\FMS;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Asropaten\FMS\Model\Model as Model;
use Illuminate\Support\Facades\View;

class Base_Model extends EloquentModel
{
    protected $model;
    public function getArray($property = "id"){
        $array = array();
        $data = $this->all();
        foreach($data as $row){
            $array[] = $row->{$property};
        }
        return $array;
    }
    public function createdAt($id){
        $data = $this->find($id);
        return $data->created_at;
    }
    public function module($view, $array = null, $return = false){
        $viewPath = "Module::" . str_replace("\\",".", $this->getModel()->getPathForModules()) . $view;
        $data = $array ? array_merge($array, [$this->getModel()->name => $this]) : [$this->getModel()->name => $this];
        if(View::exists($viewPath)) {
            if(!$return) {
                echo View::make($viewPath, $data);
            } else {
                return View::make($viewPath, $data);
            }
        } else {
            return "no view @ $viewPath";
        }
    }
    public function setModel(){
        $model = Model::where('class', get_class($this))->first();
        if($model)
            $this->model = $model;
        else
        {
            dd('Model '.get_class($this).' does not exist.');
        }
    }
    public function getModel(){
        if(!$this->model) $this->setModel();
        return $this->model;
    }
}