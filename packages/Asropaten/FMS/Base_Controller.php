<?php

namespace Asropaten\FMS;

use App\Http\Controllers\Controller;
use Asropaten\FMS\Model\Layout;
use Asropaten\FMS\Model\App;
use Asropaten\FMS\Model\FmsUser;
use Illuminate\Support\Facades\Auth;

class Base_Controller extends Controller
{
    protected $app;
    protected $package;
    protected $controller;
    protected $action;
    protected $user;
    protected $fmsUser;

    protected $layout;
    protected $view;

    private $data = array();

    public function setApp(){
        $app = App::find(1);
        $this->app = $app;
    }
    public function getApp(){
        return $this->app;
    }
    public function setPackage($package){
        $this->package = $package;
    }
    public function getPackage(){
        return $this->package;
    }
    public function setController($controller){
        $this->controller = $controller;
    }
    public function getController(){
        return $this->controller;
    }
    public function setAction($action){
        $this->action = $action;
    }
    public function getAction(){
        return $this->action;
    }
    public function setLayout(){
        $layout = Layout::find($this->package->layout_id);
    }
    public function getLayout(){
        return $this->layout;
    }
    public function setUser(){
        $this->user = Auth::guard('fms_user')->user();
    }
    public function getUser(){
        return $this->user;
    }
    public function setFmsUser(){
        $this->fmsUser = $this->getUser() ? FmsUser::find($this->getUser()->id) : false;
    }
    public function getFmsUser(){
        return $this->fmsUser;
    }
    public function addData($key, $value){
        $this->data[$key] = $value;
    }
    private function getData($type = 'object'){
        $function = 'getData'.$type;
        return $this->$function();
    }
    protected function getDataObject()
    {
        return json_decode(json_encode($this->data));
    }
    protected function getDataArray($key = false)
    {
        return $key ? $this->data[$key] : $this->data;
    }
    public function initView(){
        $viewNameSpace = $this->package->name;
        $this->view = $viewNameSpace . "::" . $this->controller->name ."." . $this->action->name . "." . $this->action->name;
    }
    public function handle($package, $controller, $action){
        $this->setApp();
        $this->setPackage($package);
        $this->setController($controller);
        $this->setAction($action);
        $this->setUser();
        $this->setFmsUser();

        $this->setLayout();
        $this->initView();

        $this->addData('app', $this->getApp());
        $this->addData('package', $this->getPackage());
        $this->addData('controller', $this->getController());
        $this->addData('action', $this->getAction());
        $this->addData('layout', $this->getLayout());
        $this->addData('user', $this->getUser());
        $this->addData('fmsUser', $this->getFmsUser());

        $method = $action->name;
        $this->$method();
        return $this->showView();
    }
    public function showView(){
        return view($this->view, $this->getData('Array'));
    }
}