<?php

namespace Asropaten\FMS_Installer;

use Illuminate\Support\ServiceProvider;

class InstallServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set views DIR
        $this->loadViewsFrom(__DIR__.'/View', 'install');
        //Publish Asset
        $this->publishes(
            [
                __DIR__.'/Asset' => public_path('Asset/Asropaten/FMS_Installer'),
            ],
            'Asset'
        );
        // Publish Data
        $this->publishes(
            [
                __DIR__.'/Data/Public' => public_path('Data/Asropaten/FMS_Installer'),
            ],
            'Data');
        // Publish Config
        $this->publishes(
            [
                __DIR__.'/Config' => config_path()
            ],
            'Config'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        include __DIR__.'/routes.php';
    }
}
