<?php

namespace Asropaten\FMS_Installer\Service;

class Enver{

    protected $path;
    protected $fileName;
    protected $variables = array();

    private $enverContent;

    //initial value of the variable
    function __construct($enverName='.env'){

        if (file_exists(base_path($enverName))){
            $this->setPath(base_path($enverName));
            $this->setFileName(substr($enverName,1));
            $this->setVariables();
            $this->setEnverContent();
        }
        else{
            die("Missing enver file.");
        }
    }

    //push variable to enver file
    public function addVariable($key,$value){

        if(!$this->existKey($key)){
            file_put_contents($this->getPath(), "\n".$key."=".$value , FILE_APPEND);
        }
        
        $this->setEnverContent();
        $this->setVariables();
    }

    //edit variable in enver file
    public function editVariable($key,$value){
        
        if($this->existKey($key)){

            $content = $this->getEnverContent();
            $token = substr($content, strpos($content, $key));
            $last_var = substr($token,0,strpos($token,"\n"));
            $new_var = $key.'='.$value;

            if($last_var != $new_var){
                file_put_contents($this->getPath(),str_replace($last_var,$new_var,$content));
            }
        }
        else{
            $this->addVariable($key,$value);
        }
        $this->setEnverContent();
        $this->setVariables();
    } 

    //remove variable in enver file
    public function deleteVariable($key){

        if($this->existKey($key)){

            $content = $this->getEnverContent();
            $token = substr($content, strpos($content, $key));
            $last_var = substr($token,0,strpos($token,"\n"))."\n";
            file_put_contents($this->getPath(),str_replace($last_var,'',$content));

            $this->setEnverContent();
            $this->setVariables();
        }
    }

    /**
     * @return string
     */
    public function getFileName(){
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName){
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getPath(){
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path){
        $this->path = $path;
    }

    /**
     * @return $enverContent
     */
    private function getEnverContent(){
        return $this->enverContent;
    }

    private function setEnverContent(){
        $this->enverContent = file_get_contents($this->getPath());
    }

    /**
     * @return $variables
     */
    public function getVariables(){
        return $this->variables;
    }

    //put all enver variables in $variables
    public function setVariables(){

        $content=file_get_contents($this->getPath());
        $token=explode("\n", $content);

        for ($i=0; $i < count($token) ; $i++){
            $vars[substr($token[$i],0,strpos($token[$i], '='))] = substr($token[$i],strpos($token[$i], '=')+1);
        }

         array_pull($vars, '');
         $this->variables = $vars;
    }


    //check key in variables exist or not!
    public function existKey($key){
        if(array_has($this->getVariables(), $key)){
            return true;
        }
        return false;
    }

}
?>