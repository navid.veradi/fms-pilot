<?php

namespace Asropaten\FMS_Installer\Service;

use GuzzleHttp;
use ZipArchive;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

class Updater {

    protected $asropatenResponse;
    protected $updateAvailable = false;
    protected $licenseValid = true;
    protected $applicationVersion;
    protected $license;

    function __construct(){
    }

    // public 'status' => string '=' (length=1)
    // public 'version' => string '0.0.62' (length=6)
    // public 'message' => string 'Your software is up to date.' (length=28)
    public function setAsropatenResponse()
    {
        if(!$this->getLicense()) {
            die('No License');
        }
        $client = new GuzzleHttp\Client();
        $res = $client->request(
            'GET',
            $this->getUpdateServer().'fmsversion', [
                'query' => [
                    'version'   => $this->getApplicationVersion(),
                    'token'     => $this->getLicense()
                ]
            ]
        );
        $body = (string) $res->getBody();
        $this->asropatenResponse = json_decode($body);
    }

    /**
     * @return mixed
     */
    public function getAsropatenResponse()
    {
        return $this->asropatenResponse;
    }

    public function pull($package)
    {
        $this->download($package);
        $this->extract($package);
        $this->vendorPublish();
        $this->migrate();
    }

    public function download($package)
    {
        $resource = fopen(base_path().'/packages/'.$package.'.zip', 'w');
        $client = new GuzzleHttp\Client();
        try {
            $client->request(
                'POST',
                $this->getUpdateServer()."getfms",
                [
                    'form_params' => [
                        'token' => $this->getLicense() . time(),
                        'package' => $package
                    ],
                    'sink' => $resource
                ]
            );
        } catch (\GuzzleHttp\Exception\ClientException $e){
            echo Psr7\str($e->getRequest());
            echo Psr7\str($e->getResponse());
            return redirect("/");
        }
    }

    public function extract($package)
    {
        $zipArchive = base_path().'/packages/'.$package.'.zip';
        $to = base_path().'/packages/'.$package;

        if (!file_exists($to)) {
            mkdir($to, 0755, true);
        }
        $zip = new ZipArchive;
        $res = $zip->open($zipArchive);
        if ($res === TRUE) {
            $zip->extractTo($to);
            $zip->close();
            File::delete($zipArchive);
            return true;
        } else {
            return false;
        }
    }

    public function vendorPublish()
    {
        // Publish FMS configuration
        Artisan::call('vendor:publish', [
            '--tag' => 'Config',
            '--force' => true
        ]);
        // Publish FMS Assets
        Artisan::call('vendor:publish', [
            '--tag' => 'Asset',
            '--force' => true
        ]);
        // Publish FMS Public Data
        Artisan::call('vendor:publish', [
            '--tag' => 'Data',
            '--force' => true
        ]);
    }

    public function migrate()
    {
        // Make new Migrations
        Artisan::call('migrate', [
            '--path' => 'packages/Asropaten/FMS/Migration/'
        ]);
    }

    /**
     * @return boolean
     */
    public function isUpdateAvailable()
    {
        return !strcmp($this->getAsropatenResponse()->status,">");
    }

    /**
     * @return mixed
     */
    public function getApplicationVersion()
    {
        return config('fms.version');
    }

    /**
     * @return mixed
     */
    public function getUpdateServer()
    {
        return config('fmsInstaller.update_server');
    }

    /**
     * @param mixed $license
     */
    public function setLicense($license)
    {
        $this->license = $license;
    }

    /**
     * @return mixed
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * @return boolean
     */
    public function isLicenseValid()
    {
        return $this->licenseValid;
    }
}
