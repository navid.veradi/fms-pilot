@extends("install::Installation.layout")

@section("content")
    <h1 class="-center">Administration Configuration</h1>
    <div class="TILES -MIDDLE">
        <div class="_TILE _1_2">

            <form class="form -padding-top-first -padding-bottom-first" method="post" action="/installation/admin_account">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <div class="_elements -padding-third TILES">
                    <div class="_element">
                        <label for="admin_email">Administrator Email</label>
                        <input id="admin_email" name="admin_email" type="text">
                    </div>
                    <div class="_element">
                        <label for="admin_password">Password</label>
                        <input id="admin_password" name="admin_password" type="password">
                    </div>
                    <div class="_element">
                        <label for="admin_password_confirmation">Confirm Password</label>
                        <input id="admin_password_confirmation" name="admin_password_confirmation" type="password">
                    </div>
                    @if($errors->has())
                        @foreach ($errors->all() as $error)
                            <p class="-red">{{ $error }}</p>
                        @endforeach
                    @endif
                    {{--/<div class="col-sm-8">
                        <button id="checkdb" class="-success">Check Database Connection</button>
                    </div>/--}}
                    <div class="_element">
                        <div class="row">
                            <div class="col-sm-4">
                                <button class="-light" onclick="" type="button">Back</button>
                            </div>
                            <div class="col-sm-8">
                                <button class="-primary">Finish</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
@endsection
