@extends("install::Installation.layout")

@section("content")
    <h1 class="-center">Site Configuration</h1>
    <div class="TILES -MIDDLE">
        <div class="_TILE _1_2">

            <form class="form -padding-top-first -padding-bottom-first" method="post" action="/installation/site">
                <div class="_elements -padding-third TILES">
                    @if($errors->has())
                        <ul style="color: red">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <div class="_element">
                        <label for="vendor">Company<span class="_help">Vendor</span></label>
                        <input id="vendor" name="vendor" type="text" value="{{$vendor}}">
                    </div>
                    <div class="_element">
                        <label for="package">Application Name<span class="_help">Package</span></label>
                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                        <input id="package" name="package" type="text" value="{{$package}}">
                    </div>
                    <p>Your Package Path: /Packages/Company/AppName</p>
                    <div class="_element">
                        <div class="row">
                            <div class="col-sm-4">
                                <button class="-light" type="button">Back</button>
                            </div>
                            <div class="col-sm-8">
                                <button class="-primary">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
@endsection