@extends("install::Installation.layout")

@section("content")
    <h1 class="-center">Database Configuration</h1>
    <div class="TILES -MIDDLE">
        <div class="_TILE _1_2">

            <form class="form -padding-top-first -padding-bottom-first" method="post" action="/installation/database">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <div class="_elements -padding-third TILES">

                    <div class="_element">
                        <label for="db_name">Database<span class="_help">DB name</span></label>
                        <input id="db_name" name="db_name" type="text">
                    </div>
                    <div class="_element">
                        <label for="db_username">Username<span class="_help">DB username</span></label>
                        <input id="db_username" name="db_username" type="text">
                    </div>
                    <div class="_element">
                        <label for="db_password">Password<span class="_help">DB password</span></label>
                        <input id="db_password" name="db_password" type="password">
                    </div>
                    @if(isset($failed))
                        @if($failed == true)
                                <p class="-red">Database Connection Failed.</p>
                        @endif
                    @endif
                    @if($errors->has())
                            @foreach ($errors->all() as $error)
                                <p class="-red">{{ $error }}</p>
                            @endforeach
                    @endif
                    {{--/<div class="col-sm-8">
                        <button id="checkdb" class="-success">Check Database Connection</button>
                    </div>/--}}
                    <div class="_element">
                        <div class="row">
                            <div class="col-sm-4">
                                <button class="-light" onclick="" type="button">Back</button>
                            </div>
                            <div class="col-sm-8">
                                <button class="-primary">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
@endsection
@section('scripts')
    <script type="text/javascript">
        $("#checkdb").click(function(){

        });
    </script>
@endsection