<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('title')
    <meta name="description" content="Loggo">
    <meta name="author" content="Milad Majidi &lt;milad.majidi@studios7.de&gt;">
    <link rel="icon" href="/Data/Asropaten/FMS_Installer/App/icon/1.ico">

    <meta name="keywords" content="KEYWORDS">
    <meta name="language" content="en">
    <meta name="url" content="http://asropaten.com">

    <link rel="stylesheet" href="/Asset/Asropaten/FMS_Installer/bootstrap.min.css">
    <link rel="stylesheet" href="/Asset/Asropaten/FMS_Installer/style.css">

<body id="asropaten-fms_installer-installation-index">

<!-- Block: navbar -->
<nav class="navbar navbar-fixed-top">
    <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
        <div class="container ">
            <div class="TILES">
                <div class="_TILE">
                    <span class="navbar-brand closed -height-fourth">
                        <img class="_logo" style="max-height:32px;width:auto;" src="/Data/Asropaten/FMS_Installer/App/logo/1.png">
                        <img class="_logo_responsive -padding-top-fifth -padding-bottom-fifth -width-sixth" style="width:auto;margin:auto;" src="/Data/Asropaten/FMS_Installer/App/logo-responsive/1.png">
                    </span>
                </div>
                <div class="_TILE _0_0 TILES -padding-left-second -padding-right-second -padding-bottom-third -padding-top-third -bg-primary -black">
                    <div class="_TILE _1_1">Installer</div>
                </div>
                <div class="_TILE -RIGHT">
                    <form class="form pull-xs-right -width-fourth -padding-right-zero -padding-third" action="/login">
                        <div class="_element">
                            <button class="-line -primary"><span>Button</span></button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</nav>

<div class="container ">
    <div class="content">
        <main class="no-sidebar -padding-top-first">
            @yield('content')
            <footer class="TILES -padding-top-second -padding-bottom-second">
                <div class="_TILE -FLEX -padding-right-fourth text-xs-right">Made with</div>
                <div class="_TILE _heart"></div>
                <div class="_TILE -FLEX -padding-left-fourth text-xs-left">in Berlin.</div>
            </footer>
        </main>
    </div>
</div>

@yield('scripts')
<script src="/Asset/Asropaten/FMS_Installer/jquery-1.11.3.min.js"></script>
<script src="/Asset/Asropaten/FMS_Installer/bootstrap.min.js"></script>
<script src="/Asset/Asropaten/FMS_Installer/tether.js"></script>

</body>
</html>