@extends("install::Installation.layout")

@section('content')
<h1 class="-center">Welcome to FMS.</h1>
<div class="TILES -MIDDLE">
    <div class="_TILE _1_2">
        <form class="form -padding-top-first -padding-bottom-first" method="post" action="/">
            <div class="_elements -padding-third TILES">
                <div class="_element">
                    <label for="license">Please enter your License-Key<span class="_help">16 Digits</span></label>
                    <input id="license" name="license" type="text">
                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                    <p class="-red">
                        Your License-Key is not valid.
                    </p>
                </div>
                <div class="_element">
                    <button class="-dark">Run the Installer</button>
                </div>
            </div>
        </form>
    </div>
</div>
<hr>
@endsection
