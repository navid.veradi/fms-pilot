<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {
    // Step 1
    Route::get(
        '/',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@index"
        );
    Route::post(
        '/',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@indexHandler"
        );
    // Step 2
    Route::get(
        '/installation/site',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@site"
        );
    Route::post(
        '/installation/site',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@siteHandler"
        );
    // Step 3
    Route::get(
        '/installation/database',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@database"
        );
    Route::post(
        '/installation/database',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@databasehandler"
        );
    Route::get(
        '/installation/database/checkConnection',
        function(){
            try {
                var_dump(DB::select('SHOW TABLES'));
            }
            catch (\PDOException $e) {
                return redirect('/installation/database?failed=true');
            }
            return redirect("/installation/admin_account");
        });
    // Step 4
    Route::get(
        '/installation/admin_account',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@adminAccount"
        );
    Route::post(
        '/installation/admin_account',
        "Asropaten\\FMS_Installer\\Controller\\InstallationController@adminAccounthandler"
        );
});