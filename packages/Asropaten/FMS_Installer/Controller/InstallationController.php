<?php

namespace Asropaten\FMS_Installer\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp;
use Asropaten\FMS_Installer\Service\Updater;
use Asropaten\FMS_Installer\Service\Enver;

class InstallationController extends Controller
{
    protected $enver;

    function __construct()
    {
        $this->enver= new Enver('.env');
    }

    // Step 1: License
    public function index()
    {
        return view("install::Installation.index.index");
    }

    public function indexHandler(Request $request)
    {
        $this->validate(
            $request,
            [
                'license' => 'required'
            ]
        );
        $updater = new Updater();
        $updater->setLicense($request->input('license'));
        $updater->pull('Asropaten/FMS');

        $this->enver->addVariable('FMS_AVAILABLE', 'TRUE');
        $this->enver->addVariable('FMS_LICENCE_KEY', $request->input('license'));

        return redirect('/installation/site');
    }

    // Step 2: Company/Vendor, Package
    public function site()
    {
        $enverVariables = $this->enver->getVariables();
        if(config("fms.license") != ""){
            $vendor = !empty($enverVariables['FMS_VENDOR']) ? $enverVariables['FMS_VENDOR'] : '';
            $package = !empty($enverVariables['FMS_PACKAGE']) ? $enverVariables['FMS_PACKAGE'] : '';
            return view(
                "install::Installation.steps.site",
                [
                    "vendor" => $vendor,
                    "package" => $package
                ]
            );
        }
        else {
            return redirect("/");
        }
    }

    public function siteHandler(Request $request)
    {
        $this->validate($request, [
            'vendor' => 'required',
            'package' => 'required'
        ]);

        $this->enver->editVariable("FMS_VENDOR", $request->input('vendor'));
        $this->enver->editVariable("FMS_PACKAGE", $request->input('package'));

        return redirect("/installation/database");
    }

    // Step3: Database
    public function database(Request $request)
    {
        if($this->enver->existKey("FMS_VENDOR")){
            return view("install::Installation.steps.database", ['failed' => $request->input('failed')]);
        }
        else {
            return redirect("/installation/site");
        }   
    }

    public function databaseHandler(Request $request)
    {

        $this->validate($request, [
            'db_name' => 'required',
            'db_username' => 'required',
            'db_password' => 'required'
        ]);

        $dbName = $request->input("db_name");
        $dbUsername = $request->input("db_username");
        $dbPassword = $request->input("db_password");

        $this->enver->editVariable("DB_DATABASE",$dbName);
        $this->enver->editVariable("DB_USERNAME",$dbUsername);
        $this->enver->editVariable("DB_PASSWORD",$dbPassword);

        return redirect('/installation/database/checkConnection');
    }

    // Step 4: Admin Account
    public function adminAccount()
    {
        if($this->enver->existKey("FMS_VENDOR")) {
            return view("install::Installation.steps.adminAccount");
        }
        else {
            return redirect("/installation/site");
        }
    }

    public function adminAccountHandler(Request $request)
    {
        $enverVariables = $this->enver->getVariables();
        // We need to decide on Authentication System and database schema.
        $this->validate($request, [
            'admin_email' => 'required|email',
            'admin_password' => 'required|confirmed',
            'admin_password_confirmation' => 'required'
        ]);

        $this->enver->addVariable("FMS_ADMIN_EMAIL", $request->input('admin_email'));
        $this->enver->addVariable("FMS_ADMIN_PASSWORD", $request->input('admin_password'));
        $this->enver->addVariable("FMS_INSTALLED", "TRUE");

        if (file_exists(base_path().'/packages/Asropaten/FMS')) {
            mkdir(base_path().'/packages/'.$enverVariables["FMS_VENDOR"].'/'.$enverVariables["FMS_PACKAGE"], 0755, true);
        }
        return redirect("/login");
    }

}