var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass(
        [
            './packages/Asropaten/FMS/Layout/Loggo/Sass/Style.sass'
        ], 'packages/Asropaten/FMS/Layout/Loggo/Asset/Style.css'
    );
    mix.copy('packages/Asropaten/FMS/Layout/Loggo/Asset/Style.css', 'public/Asset/Asropaten/FMS/Style.css');
});
elixir.Task.find('sass').watch('packages/Asropaten/FMS/Layout/Loggo/Sass/*/*.sass');