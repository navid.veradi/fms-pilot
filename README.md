# fms

## Development Environment Installation

##### 1. Prerequisites: VirtualBox, Vagrant, Composer, Git

__Notice:__ Before launching your fms environment, you must install [VirtualBox 5.x](https://www.virtualbox.org/wiki/Downloads) as well as [Vagrant](https://www.vagrantup.com/downloads.html) and [Composer](https://getcomposer.org/). All of these software packages provide easy-to-use visual installers for all popular operating systems. This installation script also assumes that you have composer installed and available globally as `composer` command. If that *is not the case* and you don't have composer installed at all, install it from [here](https://getcomposer.org/). But if you have other ways to run composer (i.e. `composer.phar`, `php composer.phar`, etc.). you should specify that by editing line 3 of `install-mac.sh` to the proper command. You also need to have  [Git](https://git-scm.com/) installed.

##### 2. Installing The Homestead Vagrant Box

```
vagrant box add laravel/homestead
```

__Notice:__ Once [VirtualBox 5.x](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html) have been installed, you should add the laravel/homestead box to your Vagrant installation using the following command in your terminal.

##### 3. Clone the project

```
git clone https://gitlab.com/navid.veradi/fms-pilot
```

__Notice__: You can also get the project in a zip file.


##### 4. Installing fms development environment

```
cd fms
bash install-mac.sh
```

__Notice:__ Run the `install-mac.sh` file via terminal with the `/.install-mac.sh` command while you're in fms root directory. This command will **ask for your application name and password** and install all your project's dependencies. The default name for the application name is: **fms**. If you get `permission denied` error while running `/.install-mac.sh` command please enter `chmod u+x install-mac.sh`.


##### 5. Running fms development environment

`vagrant up`

__Notice:__ Next run `vagrant up` in terminal while you're in fms root directory and *Viola*! everything is set up. 

##### 6. Run the Application

[http://www.fms.app](http://fms.app)

__Notice:__ To test this installation simply type [fms.app](fms.app) in your web browser to see your website root.

## FMS Installation

##### 1. Go to the Installer Page

[http://www.fms.app/installer](http://fms.app/installer)

Enter the license key provided for you and click **Run the Installer** button.

__Notice:__ After clicking **Run the installer** fms will download the fms package so it can take a minute or two :)

##### 2. Site configuration
Enter your company name which your website's package vendor will be name as, then your Application name which is the name of your package.

##### 3. Database configuration
Enter a valid database **name**, **username** and **password** and hit next. FMS will validate these credentials and take you to the final step. you will be redirected back here if these credentials are not valid and asked to re-enter valid ones.

__Notice:__ FMS will not work without a valid database connection. So please make sure that you enter everything correctly.

##### 4. Administrator account information
FMS will ask for a username and a password to set as an administrator account for managing FMS. This master account will have full access to all of FMS features and other accounts that are created in the future. So make sure to keep the credentials you enter in mind and keep it secret.

##### 5. Finish
Next click the **Finish** button and you have FMS installed. You will be redirected to the login page to sign in to your dashboard. Use the administrator account credentials that you entered to login.

## Usage

##### Connecting via SSH
You can SSH into your virtual machine by issuing the `vagrant ssh` terminal command from your fms directory.

##### Connecting to mysql database
A `fms` database is created for you in our virtual machine out of the box. You can connect to your database via Navicat or Sequel Pro using address `127.0.0.1` and port `33060`. Your database username/password is `homestead`/`secret`.


